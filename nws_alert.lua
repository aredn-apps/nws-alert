#!/usr/bin/env lua

package.path = package.path .. ";/www/cgi-bin/?.lua;../lib/?.lua"

require("nixio.fs")
local http     = require("socket.http")
local https    = require("ssl.https")
local json     = require("luci.jsonc")
local argparse = require("argparse")
local config   = require("nws_alert_config")

VERSION = "master"

function debug(msg)
    if config.debug then
        print(msg)
    end
end

local function dumpTable( t )

    local dumpTable_cache = {}
    local dumpString = ""

    local function sub_dumpTable( t, indent )
        local dumpString = ""

        if ( dumpTable_cache[tostring(t)] ) then
            dumpString = dumpString .. indent .. "*" .. tostring(t)
        else
            dumpTable_cache[tostring(t)] = true
            if ( type( t ) == "table" ) then
                for pos,val in pairs( t ) do
                    if ( type(val) == "table" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring( t ).. " {\n"
                        dumpString = dumpString .. sub_dumpTable( val, indent .. string.rep( " ", string.len(pos)+8 ) ) .. "\n"
                        dumpString = dumpString .. indent .. string.rep( " ", string.len(pos)+6 ) .. "}\n"
                    elseif ( type(val) == "string" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. '] => "' .. val .. '"' .. "\n"
                    else
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring(val) .. "\n"
                    end
                end
            else
                dumpString = dumpString .. indent..tostring(t) .. "\n"
            end
        end
        return dumpString
    end

    if ( type(t) == "table" ) then
        dumpString = dumpString .. tostring(t) .. " {\n"
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n"
        dumpString = dumpString .. "}\n"
    else
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n|"
    end

    return dumpString
end

--- Retrieve an alert from the NWS server if available
--
-- The zone parameter can be any of the accepted zone or county IDs that
-- the NWS documents at https://alerts.weather.gov/index.php
--
-- @tparam string zone NWS zone/county ID
-- @tparam string type The type(s) of alerts to retrieve
-- @treturn table NWS alert object
--
function get_alerts(zone, type)
    local alert_obj = {}

    local url = config.nws_api_url
    url = string.gsub(url, "%%ZONE%%", zone)
    url = string.gsub(url, "%%TYPE%%", type)

    debug(string.format("URL = %s", url))
    text, code, headers = https.request(url)
    text = text .. "\n"
    alert_obj = json.parse(text)
    return alert_obj
end

--- Return an alert if present
--
-- tparam table alert_obj Structured data describing alerts
-- treturn table Structure data describing first alert
--
function check_for_alert(alert_obj)
    if type(next(alert_obj['features'])) ~= "nil" then
        return alert_obj["features"][1]
    end
    return nil
end

function process_alert(event, channel)
    local text = ""

    if config.post_type == "headline" then
        text = event["properties"]["parameters"]["NWSheadline"][1]
    else
        local headline = event['properties']['headline']
        local body =  event['properties']['description']
        text = create_post(headline, body)
    end

    debug("Text to post: " .. text)

    if not config.nopost then
        debug("Sending to channel " .. channel)
        send_to_meshchat(channel, text)
    else
        debug("Not sending to MeshChat due to --nopost")
    end
end

local function urlencode(url)
    if url == nil then
        return
    end
    url = url:gsub("\n", "\r\n")
    url = url:gsub("([^%w ])", function(c)
        return string.format("%%%02X", string.byte(c))
    end)
    url = url:gsub(" ", "+")
    return url
end

function formencode(form)
	local result = {};

	if form[1] then -- Array of ordered { name, value }
		for _, field in ipairs(form) do
			table.insert(result, _urlencode(field.name).."=".. urlencode(field.value));
		end
	else -- Unordered map of name -> value
		for name, value in pairs(form) do
			table.insert(result, urlencode(name).."="..urlencode(value));
		end
	end

	return table.concat(result, "&");
end

--- Send specified text to a channel on the meshchat server.
--
-- @tparam string channel Channel name to send to
-- @tparam string text Message to the channel
--
function send_to_meshchat(channel, text)
    args = {
        action = "send_message",
        channel = channel,
        call_sign = config.sender,
        message = text,
        epoch = tostring(os.time()),
    }
    debug("args = " .. dumpTable(args))
    debug("Sending to " .. config.meshchat_url)
    resp, code, headers, error = http.request(config.meshchat_url, formencode(args))
    debug("resp = " .. dumpTable(resp))
    debug("code = " .. dumpTable(code))
    debug("headers = " .. dumpTable(resp))
    debug("error = " .. dumpTable(error))
end

--- Read in the current notification database.
--
-- @tparam string notif_file Path to notification file
-- @treturn table Notifications keyed by ID
--
function load_notifications(notif_file)
    local notifications = {}

    local json_doc = nixio.fs.readfile(notif_file)
    if json_doc then
        notifications = json.parse(json_doc)
    else
        notifications = {}
    end

    return notifications
end

--- Save all the current notifications to the database.
--
-- @tparam string notif_file Path to the notification file
-- @tparam table notifs List of notications keyed by ID
--
function save_notifications(notif_file, notifs)
    nixio.fs.writefile(notif_file, json.stringify(notifs))
end

--- Test to determine if the notification has been previously seen.
--
-- @tparam string id Notification ID
-- @treturn bool Return true if previously seen
--
function notification_seen(id)
    if type(next(NOTIFICATIONS)) ~= "nil" and NOTIFICATIONS[id] then
        return true
    end
    return false
end

--- Remove notification that are older than 24 hours
--
-- @tparam table notifications Internal representation of notifications
-- @treturn table List notifications keyed by ID
--
function prune_notifications(notifs)
    local prune_time = os.time() - 86400    -- 24 hours ago
    local current_notifs = {}

    -- copy only notifications that have occurred in the past 24 hours
    for id,notif in pairs(notifs)
    do
        if notif.time > prune_time then
            current_notifs[id] = notif
        end
    end

    return current_notifs
end

function create_post(hl, body)
    return hl .. "\n\n" .. body
end
-- =pod
-- B<create_post($headline, $body)> string
-- =cut
-- sub create_post {
--     my $hl = shift;
--     my $body = shift;

--     $body =~ s/(?!\n)\n/ /;

--     "$hl\n\n$body";
-- }

local parser = argparse('nws_alert', 'Post NWS alerts to MeshChat')
parser:flag('-d --debug', 'Debug output')
parser:flag('-x --nopost', 'Disable posting to MeshChat')
parser:option('-M --meshchat_url', 'URL used to post to MeshChat')
parser:option('-S --sender', 'Call sign to use for sending message')

-- process command line args and add them to the configuration
args = parser:parse()
for k, v in pairs(args)
do
    config[k] = v
end

NOTIFICATIONS = load_notifications(config.notification_file)
debug("load_notifications = " .. dumpTable(NOTIFICATIONS))

-- Pull the alerts for each zone
for zone_code, channel in pairs(config.alerts)
do
    debug("Checking " .. zone_code .. " for alerts")
    alert_obj = get_alerts(zone_code, 'alert,update');
    -- insure we got a valid alert_obj back
    if alert_obj then
        local alert = check_for_alert(alert_obj)
        if alert then
            local id = alert['properties']['id']
            debug("Processing " .. zone_code .. " / " .. id)

            if not notification_seen(id) then
                process_alert(alert, channel)

                -- Add to notifications
                NOTIFICATIONS[id] = { time = os.time()}
            end
        end
    end
end

NOTIFICATIONS = prune_notifications(NOTIFICATIONS)
debug("save_notifications = " .. dumpTable(NOTIFICATIONS))
save_notifications(config.notification_file, NOTIFICATIONS)
