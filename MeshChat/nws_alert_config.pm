our %ALERTS = (
    FLC127 => 'WX-Volusia',
    FLC035 => 'WX-Flagler',
    FLC069 => 'WX-Lake',
    FLC009 => 'WX-Brevard',
    );

our $SENDER = "WX_Alert_Bot";
our $MESHCHAT_URL = 'http://localnode.local.mesh:8080/cgi-bin/meshchat';
our $NOTIFICATION_FILE = '/tmp/nws-alerts-notifications';

our $DEBUG = 0;

1;