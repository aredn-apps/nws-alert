#!/bin/bash

# This script runs from the top of the project directory and
# updates the version number in the control file for the package
# being built.

IPK_DIR=$1

if [[ "$GITHUB_REF_TYPE" == 'tag' ]]; then
    # ideally should only get version tags (i.e. 'v' followed by a number)
    if [[ "${GITHUB_REF_NAME}" =~ ^v[0-9].* ]]; then
        version="${GITHUB_REF_NAME#v}"
    fi
elif [[ -n "${CI_COMMIT_TAG}" ]]; then
    # ideally should only get version tags (i.e. 'v' followed by a number)
    if [[ "${CI_COMMIT_TAG}" =~ ^v[0-9].* ]]; then
        version="${CI_COMMIT_TAG#v}"
    fi
else
    # branch gets date code-branch_name-commit
    date=$(date +%Y%m%d)
    branch=$(git rev-parse --abbrev-ref HEAD)
    commit=$(git rev-parse --short HEAD)
    version="${date}-${branch}-${commit}"
fi

sed -i "s/^Version:.*/Version: $version/" $IPK_DIR/CONTROL/control

if [[ -f $IPK_DIR/usr/local/bin/nws_alert.lua ]]; then
    sed -i "s/^VERSION.*$/VERSION  = \"${version}\"/" $IPK_DIR/usr/local/bin/nws_alert.lua
fi
